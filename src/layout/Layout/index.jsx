import Main from '../Main'
import Header from '../Header'
import Nav from '../Nav'
import Popup from '../Popup'

export default function Layout() {
    return (
        <div >
            <Main />
            <Header />
            <Nav />
            <Popup />
        </div>
    )
}