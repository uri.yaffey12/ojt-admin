import Amichai from "./Amichai"
import Avi_E from "./Avi_E"
import Avi_G from "./Avi_G"
import Boaz from "./Boaz"
import Chaim from "./Chaim"
import Daniel from "./Daniel"
import Itzchak from "./Itzchak"
import Kobi from "./Kobi"
import Moshe from "./Moshe"
import Shlomi from "./Shlomi"
import Uri from "./Uri"
import Yeshayau from "./Yeshayau"
import Orel from "./Orel"
import { Route, Routes } from "react-router-dom"


export default function MainTest() {
    return (
        <Routes>
            <Route path="/amichai" element={<Amichai />} />
            <Route path="/avi_E" element={<Avi_E />} />
            <Route path="/avi_G" element={<Avi_G />} />
            <Route path="/boaz" element={<Boaz />} />
            <Route path="/chaim" element={<Chaim />} />
            <Route path="/daniel" element={<Daniel />} />
            <Route path="/itzchak" element={<Itzchak />} />
            <Route path="/kobi" element={<Kobi />} />
            <Route path="/moshe" element={<Moshe />} />
            <Route path="/shlomi" element={<Shlomi />} />
            <Route path="/uri" element={<Uri />} />
            <Route path="/yeshayau" element={<Yeshayau />} />
            <Route path="/orel" element={<Orel />} />
        </Routes>
    )
}