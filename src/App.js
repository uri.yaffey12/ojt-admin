import './App.css';
import { BrowserRouter } from 'react-router-dom';
import Layout from './layout/Layout';
import MainTest from './test/MainTest';

export default function App() {
	return (
		<div className="App">
			<BrowserRouter>
				<Layout />
				<MainTest/>
			</BrowserRouter>
		</div>
	);
}